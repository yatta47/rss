package main

import (
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
	"text/template"

	"github.com/mmcdole/gofeed"
)

type Outline struct {
	Title   string
	HtmlUrl string
	XmlUrl  string
}

type Outlines []Outline

func generateOpml() {
	var items Outlines

	// RSSのリスト取得
	xmlLists := readXMLList()
	for _, x := range xmlLists {
		file, _ := os.Open(x)
		defer file.Close()
		fp := gofeed.NewParser()
		feed, _ := fp.Parse(file)

		// RSSの内容をパース
		item := Outline{
			Title:   feed.Title,
			HtmlUrl: feed.Link,
			XmlUrl:  feed.Link,
		}

		// 構造体の配列に詰めていく
		items = append(items, item)
	}

	// New(<ファイル名>).ParseFiles(<ひな形ファイル>)
	t, err := template.New("opml.tmpl").ParseFiles("template/opml.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	// OPMLファイルに書き出し
	writeOpmlFile(t, items)
}

func writeOpmlFile(tpl *template.Template, items Outlines) {
	outputFile := "public/all.opml"
	nf, err := os.Create(outputFile)
	if err != nil {
		log.Println("err")
	}
	defer nf.Close()

	err = tpl.Execute(nf, items)
	if err != nil {
		log.Fatal(err)
	}
}

func readXMLList() []string {
	dir := "./public"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	var lists []string
	for _, f := range files {
		if path.Ext(f.Name()) == ".xml" {
			s := []string{dir, "/", f.Name()}
			lists = append(lists, strings.Join(s, ""))
		}
	}

	return lists
}
