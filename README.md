# RSS

csvに書いたサイトからrss情報を収集して、一つにまとめるgolangのプログラム

## 使いかた

csvディレクトリの中に[検索したいワード].csvというファイルを作成して、中にrss一覧を記載する。

実行することで`public`配下にそのrssが出力される

出力されたrssは、gitlab Pagesで以下に公開される

<https://yatta47.gitlab.io/rss/XXXX.xml>

同時に、作成したrssをまとめたopmlが作成される。

opmlを読み込んで一気に登録することもできる。

<https://yatta47.gitlab.io/rss/all.opml>

## 参考にしたサイト

[Reading and writing CSV in Go. Read and write CSV files with the Go… | by Sau Sheong | Sep, 2021 | Go Recipes](https://go-recipes.dev/reading-and-writing-csv-in-go-665e996f26e0)