package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/feeds"
	"github.com/mmcdole/gofeed"
)

type csvMedia struct {
	media string
	url   string
}

func main() {
	// csvファイルのリストを取得する
	paths := readCSVList()

	var wg sync.WaitGroup
	for _, p1 := range paths {
		p := p1
		wg.Add(1)
		go func() {
			defer wg.Done()
			// csvファイルの中身を取得
			sample := readCSV(p)

			// 1csv内にあるすべてのRSSフィードのItemを取得する
			allFeedArray := getAllFeedItems(sample)

			// gofeedの形からfeedsの形に乗せ換え
			resultItems := convertItemStruct(allFeedArray)

			// feedを生成する
			rss := generateFeed(getCategory(p), resultItems)

			// feedに書き出す
			writeFeedFile(getCategory(p), rss)
		}()
	}
	wg.Wait()

	generateOpml()
}

func getCategory(p string) string {
	reg := regexp.MustCompile(`.csv$`)
	e := filepath.Base(reg.ReplaceAllString(p, ""))

	return e
}

func convertItemStruct(items []*gofeed.Item) (feedItems []*feeds.Item) {
	for _, item := range items {
		publishedTime, err := time.Parse("2006-01-02T15:04:05Z", item.PublishedParsed.Format(time.RFC3339))
		if err != nil {
			panic(err)
		}
		result := &feeds.Item{
			Title:       item.Title,
			Link:        &feeds.Link{Href: item.Link},
			Description: item.Description,
			Updated:     publishedTime,
			Created:     publishedTime,
		}
		feedItems = append(feedItems, result)
	}

	return feedItems
}

func getAllFeedItems(feeds []*csvMedia) (items []*gofeed.Item) {
	for _, s := range feeds {
		parser := gofeed.NewParser()
		feed, err := parser.ParseURL(s.url)

		fmt.Println(s.media, "check!")
		if err != nil {
			fmt.Println(s.media, ": NG")
			fmt.Fprintln(os.Stderr, err)
			continue
		}
		fmt.Println(feed.Title, ": OK")
		for _, item := range feed.Items {
			if item == nil {
				break
			}
			item.Title = "[" + s.media + "]" + item.Title
		}
		items = append(items, feed.Items...)
	}

	return
}

func generateFeed(category string, resultItems []*feeds.Item) string {
	now := time.Now()
	feedTitle := category + "のまとめフィード"
	feedLink := "https://yatta47.gitlab.io/rss/" + category + ".xml"
	feed := &feeds.Feed{
		Title:       feedTitle,
		Link:        &feeds.Link{Href: feedLink},
		Description: "説明",
		Author:      &feeds.Author{Name: "yatta47", Email: "yatta47@example.com"},
		Created:     now,
		Items:       resultItems,
	}

	rss, err := feed.ToRss()
	if err != nil {
		panic(err)
	}

	return rss
}

func writeFeedFile(feed string, content string) {
	feedName := "public/" + feed + ".xml"

	err := ioutil.WriteFile(feedName, []byte(content), 0644)
	if err != nil {
		panic(err)
	}
}

// CSVの一覧を取得
func readCSVList() []string {
	dir := "./csv"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	var lists []string
	for _, f := range files {
		if path.Ext(f.Name()) == ".csv" {
			s := []string{dir, "/", f.Name()}
			lists = append(lists, strings.Join(s, ""))
		}
	}

	return lists
}

func readCSV(path string) []*csvMedia {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var mediaList []*csvMedia
	reader := csv.NewReader(file)

	reader.Read() // csvの1行目はタイトルなので除外する

	rows, err := reader.ReadAll()
	if err != nil {
		log.Println("Cannot read CSV file:", err)
	}

	for _, r := range rows {
		c := &csvMedia{r[0], r[1]}       // 1カラム目をmediaに2カラム目はurlに入れる
		mediaList = append(mediaList, c) // csvの一覧に追加する
	}

	return mediaList
}
