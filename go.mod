module github.com/yatta47/rss-generator

go 1.15

require (
	github.com/gorilla/feeds v1.1.1
	github.com/mmcdole/gofeed v1.1.3
)
